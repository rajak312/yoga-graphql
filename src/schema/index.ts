import { createSchema } from "graphql-yoga";
import { Query } from "../resolvers/Query";

export const schema = createSchema({
  typeDefs: /* GraphQL */ `
    type Query {
      hello: String
    }
  `,
  resolvers: {
    Query,
  },
});
